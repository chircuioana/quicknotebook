package com.hb.re_membuild.models;

import android.text.Html;

import java.io.Serializable;

/**
 * Created by HB on 18.08.2017.
 */

public class Note implements Serializable {
    private String name;
    private String html;
    private String creationTimestamp;
    private String lastTimestamp;

    public Note(String name, String html, String creationTimestamp) {
        this.name = name;
        this.html = html;
        this.creationTimestamp = creationTimestamp;
        this.lastTimestamp = creationTimestamp;
    }

    public Note getDuplicate() {
        Note note = new Note(name + " copy", html, creationTimestamp);
        return note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml() {
        return html != null ? html : "";
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(String creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(String lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;

        if (name != null ? !name.equals(note.name) : note.name != null) return false;
        if (html != null ? !html.equals(note.html) : note.html != null) return false;
        if (creationTimestamp != null ? !creationTimestamp.equals(note.creationTimestamp) : note.creationTimestamp != null)
            return false;
        return lastTimestamp != null ? lastTimestamp.equals(note.lastTimestamp) : note.lastTimestamp == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (html != null ? html.hashCode() : 0);
        result = 31 * result + (creationTimestamp != null ? creationTimestamp.hashCode() : 0);
        result = 31 * result + (lastTimestamp != null ? lastTimestamp.hashCode() : 0);
        return result;
    }
}
