package com.hb.re_membuild.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hb.re_membuild.R;
import com.hb.re_membuild.listeners.OnItemClick;
import com.hb.re_membuild.models.Note;
import com.hb.re_membuild.utils.DateUtils;

import java.util.ArrayList;

/**
 * Created by HB on 24.08.2017.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<Note> notes;
    private int position;
    private OnItemClick listener;


    public NotesAdapter(Activity context, ArrayList<Note> notes, OnItemClick listener) {
        this.context = context;
        this.notes = notes;
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_note, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(notes.get(position));
    }

    @Override
    public int getItemCount() {
        if (notes != null) {
            return notes.size();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return notes.get(position).hashCode();
    }

    public void setNotes(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView tvNoteTitle;
        TextView tvNoteDate;
        RelativeLayout rlClickableSpace;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNoteTitle = (TextView) itemView.findViewById(R.id.tvNoteTitle);
            tvNoteDate = (TextView) itemView.findViewById(R.id.tvNoteDate);
            rlClickableSpace = (RelativeLayout) itemView.findViewById(R.id.rlClickSpace);
        }

        void bind(final Note note) {
            tvNoteTitle.setText(note.getName());
            tvNoteDate.setText(DateUtils.getDate(note.getLastTimestamp()));
            rlClickableSpace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(note);
                }
            });
            rlClickableSpace.setOnCreateContextMenuListener(this);
            rlClickableSpace.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    position = getAdapterPosition();
                    return false;
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuInflater inflater = context.getMenuInflater();
            inflater.inflate(R.menu.item_context_menu, menu);
        }

    }
}
