package com.hb.re_membuild.listeners;

import android.graphics.Color;

/**
 * Created by HB on 25.08.2017.
 */

public interface ColorSelectListener {

    void OnColorSelected(int color);
}
