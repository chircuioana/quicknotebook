package com.hb.re_membuild.listeners;

/**
 * Created by HB on 24.08.2017.
 */

public interface ReadObjectListener<T> {
    void onReadObjectComplete(T object);

    void onError(Exception e);
}
