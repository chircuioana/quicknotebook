package com.hb.re_membuild.listeners;

/**
 * Created by HB on 24.08.2017.
 */

public interface NameReceiveListener {
    void onName(String name);
}
