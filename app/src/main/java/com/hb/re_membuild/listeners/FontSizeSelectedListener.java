package com.hb.re_membuild.listeners;

/**
 * Created by HB on 29.08.2017.
 */

public interface FontSizeSelectedListener {

    void onFontSizeSelected(int fontSize);
}
