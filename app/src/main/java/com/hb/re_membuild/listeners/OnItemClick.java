package com.hb.re_membuild.listeners;

import com.hb.re_membuild.models.Note;

/**
 * Created by HB on 24.08.2017.
 */

public interface OnItemClick {
    void onItemClick(Note note);
}
