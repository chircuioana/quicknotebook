package com.hb.re_membuild.listeners;

/**
 * Created by HB on 24.08.2017.
 */

public interface WriteObjectListener {
    void onWriteObjectComplete();

    void onError(Exception e);
}
