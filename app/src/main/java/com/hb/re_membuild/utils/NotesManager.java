package com.hb.re_membuild.utils;

import android.content.Context;

import com.hb.re_membuild.listeners.ReadObjectListener;
import com.hb.re_membuild.listeners.WriteObjectListener;
import com.hb.re_membuild.models.Note;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by HB on 24.08.2017.
 */

public class NotesManager {

    private static final String NOTES_FILE = "NOTES_FILE";

    public static void getNotes(Context context, ReadObjectListener<HashMap<String, Note>> listener) {
        IOManager.readObject(context, context.getPackageName() + NOTES_FILE, listener);
    }

    public static void getNotesByDate(Context context, final ReadObjectListener<ArrayList<Note>> listener) {
        getNotes(context, new ReadObjectListener<HashMap<String, Note>>() {
            @Override
            public void onReadObjectComplete(HashMap<String, Note> object) {
                ArrayList<Note> notes = new ArrayList<Note>(object.values());
                Collections.sort(notes, new DateComparator());
                listener.onReadObjectComplete(notes);
            }

            @Override
            public void onError(Exception e) {
                listener.onError(e);
            }
        });
    }

    public static void saveNotes(Context context, HashMap<String, Note> notes, WriteObjectListener listener) {
        IOManager.writeObject(context, context.getPackageName() + NOTES_FILE, notes, listener);
    }

    public static void addNote(final Context context, final Note note, final WriteObjectListener listener) {
        IOManager.readObject(context, context.getPackageName() + NOTES_FILE, new ReadObjectListener<HashMap<String, Note>>() {
            @Override
            public void onReadObjectComplete(HashMap<String, Note> object) {
                if (object == null) {
                    object = new HashMap<String, Note>();
                }
                object.put(note.getName(), note);
                saveNotes(context, object, listener);
            }

            @Override
            public void onError(Exception e) {
                listener.onError(e);
            }
        });
    }

    public static void removeNote(final Context context, final Note note, final WriteObjectListener listener) {
        IOManager.readObject(context, context.getPackageName() + NOTES_FILE, new ReadObjectListener<HashMap<String, Note>>() {
            @Override
            public void onReadObjectComplete(HashMap<String, Note> object) {
                if (object == null) {
                    object = new HashMap<String, Note>();
                }
                object.remove(note.getName());
                saveNotes(context, object, listener);
            }

            @Override
            public void onError(Exception e) {
                listener.onError(e);
            }
        });
    }

    static class DateComparator implements Comparator<Note> {

        @Override
        public int compare(Note o1, Note o2) {
            return o1.getLastTimestamp().compareTo(o2.getLastTimestamp());
        }
    }
}
