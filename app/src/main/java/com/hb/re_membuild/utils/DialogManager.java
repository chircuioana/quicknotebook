package com.hb.re_membuild.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;

import com.hb.re_membuild.R;
import com.hb.re_membuild.activities.MenuActivity;
import com.hb.re_membuild.listeners.ColorSelectListener;
import com.hb.re_membuild.listeners.FontSizeSelectedListener;
import com.hb.re_membuild.listeners.NameReceiveListener;

/**
 * Created by HB on 24.08.2017.
 */

public class DialogManager {

    public static void showExitDialog(final Activity context) {
        constructYesNoDialog(context,
                "Exit",
                "Are you sure you want to without saving?",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ((MenuActivity) context).superOnBackPressed();
                    }
                },
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }
        ).show();
    }

    public static void showNameDialog(Context context, String name, final NameReceiveListener listener) {
        final EditText edittext = new EditText(context);
        if (name != null) {
            edittext.setText(name);
        }
        constructCustomViewDialog(context,
                "Choose name",
                edittext,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        listener.onName(edittext.getText().toString());
                    }
                },
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                }).show();
    }

    public static void showColorSwatches(final Context context, final ColorSelectListener listener) {
        AlertDialog dialog;
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_color_picker, null);
        dialog = constructCustomViewDialogNoButtons(
                context,
                "Select color",
                view
        ).show();

        setClickListener(view, context, R.id.icColorSwatch1, R.color.colorSwatch1, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch2, R.color.colorSwatch2, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch3, R.color.colorSwatch3, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch4, R.color.colorSwatch4, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch5, R.color.colorSwatch5, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch6, R.color.colorSwatch6, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch7, R.color.colorSwatch7, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch8, R.color.colorSwatch8, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch9, R.color.colorSwatch9, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch10, R.color.colorSwatch10, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch11, R.color.colorSwatch11, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch12, R.color.colorSwatch12, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch13, R.color.colorSwatch13, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch14, android.R.color.white, listener, dialog);
        setClickListener(view, context, R.id.icColorSwatch15, android.R.color.black, listener, dialog);
    }

    private static void setClickListener(View view, final Context context, int viewId, final int colorId, final ColorSelectListener listener, final AlertDialog finalDialog) {
        view.findViewById(viewId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalDialog.dismiss();
                listener.OnColorSelected(ContextCompat.getColor(context, colorId));
            }
        });
    }

    public static void showFontSizeDialog(Context context, final FontSizeSelectedListener listener) {
        final NumberPicker view = (NumberPicker) LayoutInflater.from(context).inflate(R.layout.dialog_font_size_picker, null);
        view.setMinValue(1);
        view.setMaxValue(30);
        constructCustomViewDialog(context,
                "Select font size",
                view,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onFontSizeSelected(view.getValue());
                        dialog.dismiss();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private static AlertDialog.Builder constructYesNoDialog(Context context,
                                                            String titleText,
                                                            String messageText,
                                                            DialogInterface.OnClickListener yesOption,
                                                            DialogInterface.OnClickListener noOption) {
        return new AlertDialog.Builder(context)
                .setTitle(titleText)
                .setMessage(messageText)
                .setPositiveButton(android.R.string.yes, yesOption)
                .setNegativeButton(android.R.string.no, noOption);
    }

    private static AlertDialog.Builder constructCustomViewDialog(Context context,
                                                                 String titleText,
                                                                 View view,
                                                                 DialogInterface.OnClickListener yesOption,
                                                                 DialogInterface.OnClickListener noOption) {
        return new AlertDialog.Builder(context)
                .setTitle(titleText)
                .setView(view)
                .setPositiveButton(android.R.string.yes, yesOption)
                .setNegativeButton(android.R.string.no, noOption);
    }

    private static AlertDialog.Builder constructCustomViewDialogNoButtons(Context context,
                                                                          String titleText,
                                                                          View view) {
        return new AlertDialog.Builder(context)
                .setTitle(titleText)
                .setView(view);
    }

}
