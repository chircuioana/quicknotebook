package com.hb.re_membuild.utils;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by HB on 24.08.2017.
 */

public class ThreadRunner {
    private static ThreadRunner instance;
    private ThreadPoolExecutor mThreadPool;

    private ThreadRunner() {
        //The queue (blocking queue) in which the threads will be held when more requests than the maximum number of threads occur.
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();
        //Keep alive time unit = seconds
        TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
        //How many time units should a thread be kept in stand-by state after it's finished it's job.
        int KEEP_ALIVE_TIME = 1;
        //The number of cores the device has = the maximum number of threads the manager will create simultaneously.
        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        mThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,
                NUMBER_OF_CORES,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                workQueue
        );
    }

    public synchronized static ThreadRunner getInstance() {
        if (instance == null) {
            instance = new ThreadRunner();
        }
        return instance;
    }

    public synchronized void execute(final Runnable runnable) {
        mThreadPool.execute(runnable);
    }

}
