package com.hb.re_membuild.utils;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by HB on 24.08.2017.
 */

public class DateUtils {

    public static String getDate(String timestamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(timestamp));
        return DateFormat.format("dd MMM yyyy", cal).toString();
    }
}
