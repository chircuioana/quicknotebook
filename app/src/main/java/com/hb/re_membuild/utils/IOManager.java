package com.hb.re_membuild.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.hb.re_membuild.listeners.ReadObjectListener;
import com.hb.re_membuild.listeners.WriteObjectListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by HB on 24.08.2017.
 */

class IOManager {

    static synchronized void writeObject(@NonNull final Context context,
                                         @NonNull final String fileName,
                                         final Object object,
                                         @NonNull final WriteObjectListener listener) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(object);
                    os.close();
                    fos.close();
                    listener.onWriteObjectComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onError(e);
                }
            }
        };
        ThreadRunner.getInstance().execute(runnable);
    }

    static synchronized <T> void readObject(@NonNull final Context context,
                                            @NonNull final String fileName,
                                            @NonNull final ReadObjectListener<T> listener) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Object result = null;
                try {
                    FileInputStream fis = context.openFileInput(fileName);
                    ObjectInputStream is = new ObjectInputStream(fis);
                    result = is.readObject();
                    is.close();
                    fis.close();
                    listener.onReadObjectComplete((T) result);
                } catch (FileNotFoundException e1) {
                    listener.onReadObjectComplete(null);
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onError(e);
                }
            }
        };
        ThreadRunner.getInstance().execute(runnable);
    }
}
