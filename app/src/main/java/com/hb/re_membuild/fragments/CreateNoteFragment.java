package com.hb.re_membuild.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.hb.re_membuild.Constants;
import com.hb.re_membuild.R;
import com.hb.re_membuild.activities.MenuActivity;
import com.hb.re_membuild.listeners.ColorSelectListener;
import com.hb.re_membuild.listeners.FontSizeSelectedListener;
import com.hb.re_membuild.listeners.NameReceiveListener;
import com.hb.re_membuild.listeners.OnNoteSavedListener;
import com.hb.re_membuild.listeners.WriteObjectListener;
import com.hb.re_membuild.models.Note;
import com.hb.re_membuild.utils.DialogManager;
import com.hb.re_membuild.utils.NotesManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.richeditor.RichEditor;

import static android.R.string.no;

/**
 * Created by HB on 18.08.2017.
 */

public class CreateNoteFragment extends Fragment implements WriteObjectListener, View.OnClickListener {

    public static CreateNoteFragment newInstance(Note note) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.NOTE_KEY, note);
        CreateNoteFragment fragment = new CreateNoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    static final String HTML_KEY = "HTML_KEY";

    @BindView(R.id.reTextEditor)
    RichEditor reTextEditor;

    @BindView(R.id.ivBold)
    ImageView ivBold;

    @BindView(R.id.ivItalic)
    ImageView ivItalic;

    @BindView(R.id.ivUnderline)
    ImageView ivUnderline;

    @BindView(R.id.ivStrikethrough)
    ImageView ivStrikethrough;

    @BindView(R.id.ivSuperscript)
    ImageView ivSuperscript;

    @BindView(R.id.ivSubscript)
    ImageView ivSubscript;

    @BindView(R.id.ivHeading1)
    ImageView ivHeading1;

    @BindView(R.id.ivHeading2)
    ImageView ivHeading2;

    @BindView(R.id.ivHeading3)
    ImageView ivHeading3;

    @BindView(R.id.ivHeading4)
    ImageView ivHeading4;

    @BindView(R.id.ivHeading5)
    ImageView ivHeading5;

    @BindView(R.id.ivHeading6)
    ImageView ivHeading6;

    @BindView(R.id.ivTextColor)
    ImageView ivTextColor;

    @BindView(R.id.ivBackgroundColor)
    ImageView ivBackgroundColor;

    @BindView(R.id.ivFontSize)
    ImageView ivFontSize;

    @BindView(R.id.ivAlignLeft)
    ImageView ivAlignLeft;

    @BindView(R.id.ivAlignCenter)
    ImageView ivAlignCenter;

    @BindView(R.id.ivAlignRight)
    ImageView ivAlignRight;

    @BindView(R.id.ivListBullet)
    ImageView ivListBullet;

    @BindView(R.id.ivListNumber)
    ImageView ivListNumber;

    private Note note;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_brick, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        init(savedInstanceState);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(HTML_KEY, reTextEditor.getHtml());
    }

    public void save(final OnNoteSavedListener listener) {
        DialogManager.showNameDialog(getActivity(),
                note != null ? note.getName() : "",
                new NameReceiveListener() {
                    @Override
                    public void onName(String name) {
                        NotesManager.addNote(getActivity(),
                                new Note(name, reTextEditor.getHtml(), String.valueOf(System.currentTimeMillis())),
                                new WriteObjectListener() {
                                    @Override
                                    public void onWriteObjectComplete() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(), "Note saved", Toast.LENGTH_SHORT).show();
                                                listener.onNoteSaved();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                        );
                    }
                });
    }

    private void init(Bundle savedInstanceState) {
        note = (Note) getArguments().get(Constants.NOTE_KEY);
        if (savedInstanceState != null) {
            String html = savedInstanceState.getString(HTML_KEY);
            reTextEditor.setHtml(html);
        } else {
            if (note != null) {
                reTextEditor.setHtml(note.getHtml().toString());
            }
        }

        reTextEditor.setEditorFontSize(22);
        reTextEditor.setEditorFontColor(-16777216);
        reTextEditor.setPadding(10, 10, 10, 10);
        setMultipleOnClickListener(this,
                new View[]{
                        ivBold,
                        ivItalic,
                        ivUnderline,
                        ivStrikethrough,
                        ivSuperscript,
                        ivSubscript,
                        ivHeading1,
                        ivHeading2,
                        ivHeading3,
                        ivHeading4,
                        ivHeading5,
                        ivHeading6,
                        ivTextColor,
                        ivBackgroundColor,
                        ivFontSize,
                        ivAlignLeft,
                        ivAlignCenter,
                        ivAlignRight,
                        ivListBullet,
                        ivListNumber});

    }

    private void setMultipleOnClickListener(View.OnClickListener listener, View[] views) {
        for (View view : views) {
            view.setOnClickListener(listener);
        }
    }

    @Override
    public void onWriteObjectComplete() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Note saved", Toast.LENGTH_SHORT).show();
                ((MenuActivity) getActivity()).superOnBackPressed();
            }
        });
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBold:
                reTextEditor.setBold();
                break;
            case R.id.ivItalic:
                reTextEditor.setItalic();
                break;
            case R.id.ivUnderline:
                reTextEditor.setUnderline();
                break;
            case R.id.ivAlignLeft:
                reTextEditor.setAlignLeft();
                break;
            case R.id.ivAlignCenter:
                reTextEditor.setAlignCenter();
                break;
            case R.id.ivAlignRight:
                reTextEditor.setAlignRight();
                break;
            case R.id.ivListBullet:
                reTextEditor.setBullets();
                break;
            case R.id.ivListNumber:
                reTextEditor.setNumbers();
                break;
            case R.id.ivStrikethrough:
                reTextEditor.setStrikeThrough();
                break;
            case R.id.ivSuperscript:
                reTextEditor.setSuperscript();
                break;
            case R.id.ivSubscript:
                reTextEditor.setSubscript();
                break;
            case R.id.ivHeading1:
                reTextEditor.setHeading(1);
                break;
            case R.id.ivHeading2:
                reTextEditor.setHeading(2);
                break;
            case R.id.ivHeading3:
                reTextEditor.setHeading(3);
                break;
            case R.id.ivHeading4:
                reTextEditor.setHeading(4);
                break;
            case R.id.ivHeading5:
                reTextEditor.setHeading(5);
                break;
            case R.id.ivHeading6:
                reTextEditor.setHeading(6);
                break;
            case R.id.ivTextColor:
                DialogManager.showColorSwatches(getActivity(), new ColorSelectListener() {
                    @Override
                    public void OnColorSelected(int color) {
                        reTextEditor.setTextColor(color);
                    }
                });
                break;
            case R.id.ivBackgroundColor:
                DialogManager.showColorSwatches(getActivity(), new ColorSelectListener() {
                    @Override
                    public void OnColorSelected(int color) {
                        reTextEditor.setTextBackgroundColor(color);
                    }
                });
                break;
            case R.id.ivFontSize:
                DialogManager.showFontSizeDialog(getActivity(), new FontSizeSelectedListener() {
                    @Override
                    public void onFontSizeSelected(int fontSize) {
                        reTextEditor.setFontSize(fontSize);
                    }
                });
                break;
        }
    }

    public Note getNote() {
        return note;
    }
}
