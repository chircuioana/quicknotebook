package com.hb.re_membuild.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.hb.re_membuild.R;
import com.hb.re_membuild.models.Note;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.data;

/**
 * Created by HB on 24.08.2017.
 */

public class ViewNoteFragment extends Fragment {

    public static final String NOTE_KEY = "NOTE_KEY";

    public static ViewNoteFragment newInstance(Note note) {
        Bundle args = new Bundle();
        args.putSerializable(NOTE_KEY, note);
        ViewNoteFragment fragment = new ViewNoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.wvViewNote)
    WebView wvViewNote;

    private Note note;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_note, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        init();
        return view;
    }

    private void init() {
        note = (Note) getArguments().get(NOTE_KEY);
        wvViewNote.getSettings().setJavaScriptEnabled(true);
        wvViewNote.loadDataWithBaseURL("", note.getHtml().toString(), "text/html", "UTF-8", "");
    }

    public Note getNote() {
        return note;
    }
}
