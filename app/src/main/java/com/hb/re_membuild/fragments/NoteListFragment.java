package com.hb.re_membuild.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hb.re_membuild.Constants;
import com.hb.re_membuild.R;
import com.hb.re_membuild.activities.MenuActivity;
import com.hb.re_membuild.adapters.NotesAdapter;
import com.hb.re_membuild.listeners.OnItemClick;
import com.hb.re_membuild.listeners.ReadObjectListener;
import com.hb.re_membuild.listeners.WriteObjectListener;
import com.hb.re_membuild.models.Note;
import com.hb.re_membuild.utils.NotesManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HB on 24.08.2017.
 */

public class NoteListFragment extends Fragment implements View.OnClickListener, ReadObjectListener<ArrayList<Note>>, SwipeRefreshLayout.OnRefreshListener {

    public static NoteListFragment newInstance() {
        Bundle args = new Bundle();
        NoteListFragment fragment = new NoteListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.rvNotesList)
    RecyclerView rvNotesList;

    @BindView(R.id.fabAddNote)
    FloatingActionButton fabAddNote;

    @BindView(R.id.srlRefresh)
    SwipeRefreshLayout srlRefresh;

    @BindView(R.id.noNotesLayout)
    View noNotesLayout;

    private NotesAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes_list, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        init();
        getNotes();
        return view;
    }

    private void init() {
        fabAddNote.setOnClickListener(this);

        adapter = new NotesAdapter(getActivity(), new ArrayList<Note>(), new OnItemClick() {
            @Override
            public void onItemClick(Note note) {
                ((MenuActivity) getActivity()).showDetailFragment(null, Constants.FragmentType.VIEW_NOTE, note, true);
            }
        });
        rvNotesList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNotesList.setLayoutManager(linearLayoutManager);
        rvNotesList.setAdapter(adapter);
        registerForContextMenu(rvNotesList);

        srlRefresh.setOnRefreshListener(this);
    }

    private void getNotes() {
        NotesManager.getNotesByDate(getActivity(), this);
    }

    private void setNotes(final ArrayList<Note> object) {
        if (object != null && object.size() > 0) {
            adapter.setNotes(object);
            adapter.notifyDataSetChanged();
            noNotesLayout.setVisibility(View.GONE);
        } else {
            adapter.setNotes(object);
            adapter.notifyDataSetChanged();
            noNotesLayout.setVisibility(View.VISIBLE);
        }
        srlRefresh.setRefreshing(false);
    }

    public void contextMenuClick(Constants.ContextMenuList contextMenu) {
        int position = adapter.getPosition();
        WriteObjectListener listener = new WriteObjectListener() {
            @Override
            public void onWriteObjectComplete() {
                getNotes();
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }
        };
        switch (contextMenu) {
            case DELETE:
                NotesManager.removeNote(getActivity(),
                        adapter.getNotes().get(position),
                        listener);
                break;
            case DUPLICATE:
                NotesManager.addNote(getActivity(),
                        adapter.getNotes().get(position).getDuplicate(),
                        listener);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAddNote:
                ((MenuActivity) getActivity()).showDetailFragment(null, Constants.FragmentType.CREATE_NOTE, null, true);
                break;
        }
    }

    @Override
    public void onReadObjectComplete(final ArrayList object) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setNotes(object);
            }
        });
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onRefresh() {
        getNotes();
    }
}
