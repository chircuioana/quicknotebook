package com.hb.re_membuild.activities;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.hb.re_membuild.Constants;
import com.hb.re_membuild.R;
import com.hb.re_membuild.fragments.CreateNoteFragment;
import com.hb.re_membuild.fragments.NoteListFragment;
import com.hb.re_membuild.fragments.ViewNoteFragment;
import com.hb.re_membuild.listeners.OnNoteSavedListener;
import com.hb.re_membuild.listeners.WriteObjectListener;
import com.hb.re_membuild.models.Note;
import com.hb.re_membuild.utils.DialogManager;
import com.hb.re_membuild.utils.NotesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.fragment;
import static com.hb.re_membuild.Constants.FragmentType.CREATE_NOTE;
import static com.hb.re_membuild.Constants.FragmentType.NONE;
import static com.hb.re_membuild.Constants.FragmentType.VIEW_NOTE;

public class MenuActivity extends AppCompatActivity implements OnNoteSavedListener {

    public static final String TAG_FRAGMENT = "RETAIN_FRAGMENT";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Note selectedNote;

    private Constants.FragmentType fragmentType;

    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.activity_menu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        fragmentType = NONE;
        init();
    }

    private void init() {
        setupToolbar();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT + "_" + ViewNoteFragment.class.getSimpleName());
        if (fragment != null) {
            Note selectedNote = ((ViewNoteFragment) fragment).getNote();
            showDetailFragment(fragment, VIEW_NOTE, selectedNote, false);
            return;
        }
        fragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT + "_" + CreateNoteFragment.class.getSimpleName());
        if (fragment != null) {
            Note selectedNote = ((CreateNoteFragment) fragment).getNote();
            showDetailFragment(fragment, CREATE_NOTE, selectedNote, false);
            return;
        }
        showFragment(R.id.flListFragment, NoteListFragment.newInstance());
    }

    private void showFragment(int layoutId, Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(layoutId, fragment, TAG_FRAGMENT + "_" + fragment.getClass().getSimpleName());
        transaction.commitNow();
        invalidateOptionsMenu();
    }

    public void showDetailFragment(Fragment fragment, Constants.FragmentType fragmentType, Note note, boolean shouldClose) {
        if (shouldClose) {
            if (!closeCurrentDetailFragment(fragmentType, note)) {
                return;
            }
        }
        this.fragmentType = fragmentType;
        this.selectedNote = note;
        if (fragment == null) {
            switch (fragmentType) {
                case VIEW_NOTE:
                default:
                    fragment = ViewNoteFragment.newInstance(note);
                    break;
                case CREATE_NOTE:
                    fragment = CreateNoteFragment.newInstance(note);
                    break;
            }
        }
        if (findViewById(R.id.flDetailFragment) != null) {
            showFragment(R.id.flDetailFragment, fragment);
        } else {
            showFragment(R.id.flListFragment, fragment);
        }
        toolbar.setTitle(selectedNote != null ? selectedNote.getName() : "");
    }

    private boolean closeCurrentDetailFragment(final Constants.FragmentType fragmentType, final Note note) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flDetailFragment);
        if (fragment != null && fragment instanceof CreateNoteFragment) {
            ((CreateNoteFragment) fragment).save(new OnNoteSavedListener() {
                @Override
                public void onNoteSaved() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MenuActivity.this.onNoteSaved();
                            showDetailFragment(null, fragmentType, note, false);
                        }
                    });
                }
            });
            return false;
        }
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Constants.ContextMenuList contextMenu;
        switch (item.getItemId()) {
            case R.id.delete:
                contextMenu = Constants.ContextMenuList.DELETE;
                break;
            case R.id.duplicate:
            default:
                contextMenu = Constants.ContextMenuList.DUPLICATE;
                break;
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flListFragment);
        if (fragment != null && fragment instanceof NoteListFragment) {
            ((NoteListFragment) fragment).contextMenuClick(contextMenu);
        }

        return super.onContextItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        showHomeButton(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void showHomeButton(boolean show) {
        if (show) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        switch (fragmentType) {
            case CREATE_NOTE:
                inflater.inflate(R.menu.save_menu, menu);
                showHomeButton(true);
                break;
            case VIEW_NOTE:
                inflater.inflate(R.menu.edit_menu, menu);
                showHomeButton(true);
                break;
            case NONE:
            default:
                toolbar.setTitle(" ");
                showHomeButton(false);
                break;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.save:
                ((CreateNoteFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT + "_" + CreateNoteFragment.class.getSimpleName())).save(this);
                break;
            case R.id.edit:
                showDetailFragment(null, CREATE_NOTE, selectedNote, true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (fragmentType == CREATE_NOTE) {
            DialogManager.showExitDialog(this);
        } else {
            superOnBackPressed();
        }
    }

    public void superOnBackPressed() {
        if (findViewById(R.id.flDetailFragment) != null) {
            if (getSupportFragmentManager().findFragmentById(R.id.flDetailFragment) != null) {
                removeDetailFragment();
            } else {
                MenuActivity.super.onBackPressed();
            }
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flListFragment);
            if (fragment instanceof ViewNoteFragment || fragment instanceof CreateNoteFragment) {
                fragmentType = NONE;
                showFragment(R.id.flListFragment, NoteListFragment.newInstance());
            } else {
                MenuActivity.super.onBackPressed();
            }
        }
    }

    @Override
    public void onNoteSaved() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (findViewById(R.id.flDetailFragment) != null) {
                    removeDetailFragment();
                    ((NoteListFragment) getSupportFragmentManager().findFragmentById(R.id.flListFragment)).onRefresh();
                } else {
                    fragmentType = NONE;
                    showFragment(R.id.flListFragment, NoteListFragment.newInstance());
                }
            }
        });

    }

    private void removeDetailFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.remove(getSupportFragmentManager().findFragmentById(R.id.flDetailFragment));
        ft.commitNow();
        fragmentType = NONE;
        invalidateOptionsMenu();
    }
}
