package com.hb.re_membuild;

/**
 * Created by HB on 24.08.2017.
 */

public class Constants {

    public enum FragmentType {
        CREATE_NOTE,
        VIEW_NOTE,
        NONE
    }

    public enum ContextMenuList {
        DELETE,
        DUPLICATE
    }

    public static final String NOTE_KEY = "NOTE_KEY";
    public static final String FRAGMENT_TYPE = "FRAGMNET_TYPE";

}
